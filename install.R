# Install R packages
# For now, this is just for a reference.
# Some OS libraries should be installed, too.

# (install from CRAN)
packages_cran <-
    c("devtools",
      "tidyverse",
      "jsonlite",
      "padr",
      "EpiNow2"
      )
install.packages(packages_cran)

# (install from github)
# EpiNow2 dev version?
